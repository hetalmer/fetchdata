/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
//1. Fetch all the users
function fetchAllUser(path) {
	//fetch the data from the path it resturns promise
	fetch(path)
		.then((response) => response.json())
		.then((data) => console.log(data))
		.catch((err) => console.log("error is", err));
}

//fetchAllUser("https://jsonplaceholder.typicode.com/users");

//2. Fetch all the todos
function fetchAllTodos(path) {
	//fetch the data from the path it returns promise
	fetch(path)
		.then((response) => response.json())
		.then((data) => console.log(data))
		.catch((err) => console.log(err));
}
//const todos = fetchAllTodos("https://jsonplaceholder.typicode.com/todos");
//3. use the promises chain and fetch the users first and then the todos.

function fetchUserTodos(userpath, todospath) {
	//get first user and then todos
	fetch(userpath)
		.then((response) => {
			response.json();
		})
		.then((data) => {
			console.log(data);
		})
		.catch((err) => {
			console.log(`geneating error in fetching data of user ${err}`);
		})
		.then(() => {
			return fetch(todospath);
		})
		.then((response) => {
			response.json();
		})
		.then((data) => {
			console.log(data);
		})
		.catch((err) => {
			console.log(`geneating error in fetching data of todos ${err}`);
		});
}
/*fetchAllTodos(
	"https://jsonplaceholder.typicode.com/users",
	"https://jsonplaceholder.typicode.com/todos",
);*/
//4. Use the promise chain and fetch the users first and then all the details for each user.
function fetchUserById(userpath) {
	let fetchPro = [];
	fetch(userpath)
		.then((response) => response.json())
		.then((data) => {
			Object.keys(data).forEach((element) => {
				fetchPro.push(
					fetch(
						`https://jsonplaceholder.typicode.com/users?id=${data[element].id}`,
					),
				);
			});
			return Promise.all(fetchPro);
		})
		.then((result) => console.log(result))
		.catch((err) => console.log("error is", err));
}
fetchUserById("https://jsonplaceholder.typicode.com/users");
